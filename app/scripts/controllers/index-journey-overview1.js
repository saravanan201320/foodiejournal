angular
  .module("followUp",["ngStorage"])

.controller('JourneyController', JourneyController)
  .factory('journeyService', journeyFactory)
  .filter('regionalFood', regionalFood)
  .controller('MainCtrl', function($scope) {
    $scope.i = 0;
    $scope.isAmericaActive = true;
    $scope.isItalyActive = false;
    $scope.isKoreaActive = false;
    $scope.isJapanActive = false;
    $scope.isMexicoActive = false;


    $scope.toggleActive1 = function(btn) {
      $scope.i = 0;
      $scope.isAmericaActive=false;
      $scope.isItalyActive = false;
      $scope.isKoreaActive = false;
      $scope.isJapanActive = false;
      $scope.isMexicoActive = false;
      if(btn === 'America') {
        $scope.isAmericaActive = true;
        $scope.i=0;
      }
      else if(btn === 'Korea' ) {
        $scope.isKoreaActive = true;
        $scope.i=1;
      }
      else if(btn === 'Italy') {
        $scope.isItalyActive =true;
        $scope.i=2;
      }
      else if(btn === 'Mexico') {
        $scope.isMexicoActive =true;
        $scope.i=3;
      }
      else if(btn === 'Japan') {
        $scope.isJapanActive =true;
        $scope.i=4;
      }
      else {
        $scope.isAmericaActive = false;
        $scope.isItalyActive = false;
        $scope.isKoreaActive = false;
        $scope.isMexicoActive = false;
        $scope.isJapanActive = false;
      }
    };

    $scope.toggleActive2 = function() {
      if($scope.isUploadActive) {
        $scope.isUploadActive = !$scope.isUploadActive;
      }
      $scope.isDownloadActive = !$scope.isDownloadActive;
    };

  }).directive('detailsInfo', function() {
    return {
      restrict: 'E',
      template: '<div ><div ng-transclude></div></div>',
      transclude: true
    };
  });



var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
  });
}

var dataFood;
function regionalFood () {
  return function(films,String) {
    var region;
    if(String)
      region ="America";
    debugger;
    var val=[];
    if(films!=null) {
      for (var i = 0; i < 5; i++) {
        var checkString = films[i];
        if (checkString === region) {
          debugger;
          if (films[i] != null) {
            val[i] = films[i];
            console.log(val[i]);

          }
        }
      }
    }

    return val;
  }

}



function journeyFactory($http) {
  this.$inject = ['$http'];

  return {
    getFilms: getFilms,
    regionalFood:regionalFood
  };

  function getFilms () {
    return $http
      .get('./food.json')
      .then(complete)
      .catch(failed);
  }

  function complete(response) {
    return response.data;
  }

  function failed(error) {
    return error.statusText;
  }
}

// we are injecting FilmService
function JourneyController($scope, journeyService) {
  this.$inject = ['$scope', 'journeyService'];
  journeyService.getFilms().then(function (data) {
    $scope.films = data;
    this.dataFood =data;
  });


}

