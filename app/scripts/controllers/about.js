'use strict';

/**
 * @ngdoc function
 * @name gurgaonAugustTeamApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the gurgaonAugustTeamApp
 */
angular.module('gurgaonAugustTeamApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
