/* global $*/
"use strict";
function userFactory($http) {
  /*jshint validthis:true*/  this.$inject = ['$http'];

  function getUser() {
    return $http
      .get('../data/user.json')
      .then(complete)
      .catch(failed);
  }

  function complete(response) {
    return response.data;
  }

  function failed(error) {
    return error.statusText;
  }

  return {
    getUser: getUser
         };
}

function call(data) {
  var WITHOUT = data.replace('%','');
  $('head style[type="text/css"]').attr('type', 'text/less');
  var TRANSFER_STYLES = ['-webkit-transform', '-ms-transform', 'transform'];
  window.randomize = function() {
    var FILL_ROTATION = WITHOUT * (1.8);
                    for (var i in TRANSFER_STYLES) {
                              $('.circle .fill, .circle .mask.full').css(TRANSFER_STYLES[i], 'rotate(' + FILL_ROTATION + 'deg)');
                                   }
  };
  setTimeout(window.randomize, 200);
  $('.radial-progress').click(window.randomize);
}
// we are injecting FilmService
function JourneyController($scope, journeyService, userService, $localStorage) {
  this.$inject = ['$scope', 'journeyService','userService','$localStorage'];
  //$localStorage.$reset();
  $scope.i = 0;
  $scope.isAmericaActive = true;
  $scope.isItalyActive = false;
  $scope.isKoreaActive = false;
  $scope.isJapanActive = false;
  $scope.isMexicoActive = false;
  $scope.SelectionText = null;





  userService.getUser().then(function(data) {
    $scope.users = data;
  });


  journeyService.getFoodDetails().then(function(data) {
    $scope.films = data;
    $localStorage.foodDetails = data;

    $scope.AmericaindexNumber0 = true;
    $scope.AmericacompleteImage0 = false;

    $scope.AmericaindexNumber1 = true;
    $scope.AmericacompleteImage1 = false;

    $scope.AmericaindexNumber2 = true;
    $scope.AmericacompleteImage2 = false;

    $scope.AmericaindexNumber3 = true;
    $scope.AmericacompleteImage3 = false;

    $scope.AmericaindexNumber4 = true;
    $scope.AmericacompleteImage4 = false;

    $scope.KoreaindexNumber0 = true;
    $scope.KoreacompleteImage0 = false;

    $scope.KoreaindexNumber1 = true;
    $scope.KoreacompleteImage1 = false;

    $scope.KoreaindexNumber2 = true;
    $scope.KoreacompleteImage2 = false;

    $scope.KoreaindexNumber3 = true;
    $scope.KoreacompleteImage3 = false;

    $scope.KoreaindexNumber4 = true;
    $scope.KoreacompleteImage4 = false;

    $scope.italyindexNumber0 = true;
    $scope.italycompleteImage0 = false;

    $scope.italyindexNumber1 = true;
    $scope.italycompleteImage1 = false;

    $scope.italyindexNumber2 = true;
    $scope.italycompleteImage2 = false;

    $scope.italyindexNumber3 = true;
    $scope.italycompleteImage3 = false;

    $scope.italyindexNumber4 = true;
    $scope.italycompleteImage4 = false;

    $scope.japaneseindexNumber0 = true;
    $scope.japanesecompleteImage0 = false;

    $scope.japaneseindexNumber1 = true;
    $scope.japanesecompleteImage1 = false;

    $scope.japaneseindexNumber2 = true;
    $scope.japanesecompleteImage2 = false;

    $scope.japaneseindexNumber3 = true;
    $scope.japanesecompleteImage3 = false;

    $scope.japaneseindexNumber4 = true;
    $scope.japanesecompleteImage4 = false;

    $scope.mexicanindexNumber0 = true;
    $scope.mexicancompleteImage0 = false;

    $scope.mexicanindexNumber1 = true;
    $scope.mexicancompleteImage1 = false;

    $scope.mexicanindexNumber2 = true;
    $scope.mexicancompleteImage2 = false;

    $scope.mexicanindexNumber3 = true;
    $scope.mexicancompleteImage3 = false;

    $scope.mexicanindexNumber4 = true;
    $scope.mexicancompleteImage4 = false;

   //console.log("-------------------------------"+$localStorage.abtnName);

    if($localStorage.abtnName == "America"){

      if ($localStorage.aFirstFood == "completed") {
        console.log($localStorage.foodDetails[0]);
        $scope.AmericaindexNumber0 = false;
        $scope.AmericacompleteImage0 = true;
      }

      if ($localStorage.aSecondFood == "completed") {
        $scope.AmericaindexNumber1 = false;
        $scope.AmericacompleteImage1 = true;
      }

      if ($localStorage.aThirdFood == "completed") {
        $scope.AmericaindexNumber2 = false;
        $scope.AmericacompleteImage2 = true;

      }

      if ($localStorage.aFourthFood == "completed") {
        $scope.AmericaindexNumber3 = false;
        $scope.AmericacompleteImage3 = true;
      }
      if ($localStorage.aFifthFood == "completed") {
        $scope.AmericaindexNumber4 = false;
        $scope.AmericacompleteImage4 = true;
      }

    }
    if($localStorage.kbtnName == "Korea"){
      if ($localStorage.kFirstFood == "completed") {
        console.log($localStorage.foodDetails[0]);
        $scope.KoreaindexNumber0 = false;
        $scope.KoreacompleteImage0 = true;
      }

      if ($localStorage.kSecondFood == "completed") {
        $scope.KoreaindexNumber1 = false;
        $scope.KoreacompleteImage1 = true;
      }

      if ($localStorage.kThirdFood == "completed") {
        $scope.KoreaindexNumber2 = false;
        $scope.KoreacompleteImage2 = true;

      }

      if ($localStorage.kFourthFood == "completed") {
        $scope.KoreaindexNumber3 = false;
        $scope.KoreacompleteImage3 = true;
      }
      if ($localStorage.kFifthFood == "completed") {
        $scope.KoreaindexNumber4 = false;
        $scope.KoreacompleteImage4 = true;
      }
    }

    if($localStorage.ibtnName == "italy"){
      if ($localStorage.iFirstFood == "completed") {
        console.log($localStorage.foodDetails[0]);
        $scope.italyindexNumber0 = false;
        $scope.italycompleteImage0 = true;
      }

      if ($localStorage.iSecondFood == "completed") {
        $scope.italyindexNumber1 = false;
        $scope.italycompleteImage1 = true;
      }

      if ($localStorage.iThirdFood == "completed") {
        $scope.italyindexNumber2 = false;
        $scope.italycompleteImage2 = true;

      }

      if ($localStorage.iFourthFood == "completed") {
        $scope.italyindexNumber3 = false;
        $scope.italycompleteImage3 = true;
      }
      if ($localStorage.iFifthFood == "completed") {
        $scope.italyindexNumber4 = false;
        $scope.italycompleteImage4 = true;
      }
    }

    if($localStorage.jbtnName == "japanese"){
      if ($localStorage.jFirstFood == "completed") {
        console.log($localStorage.foodDetails[0]);
        $scope.japaneseindexNumber0 = false;
        $scope.japanesecompleteImage0 = true;
      }

      if ($localStorage.jSecondFood == "completed") {
        $scope.japaneseindexNumber1 = false;
        $scope.japanesecompleteImage1 = true;
      }

      if ($localStorage.jThirdFood == "completed") {
        $scope.japaneseindexNumber2 = false;
        $scope.japanesecompleteImage2 = true;

      }

      if ($localStorage.jFourthFood == "completed") {
        $scope.japaneseindexNumber3 = false;
        $scope.japanesecompleteImage3 = true;
      }
      if ($localStorage.jFifthFood == "completed") {
        $scope.japaneseindexNumber4 = false;
        $scope.japanesecompleteImage4 = true;
      }
    }

    if($localStorage.mbtnName == "mexican"){
      if ($localStorage.mFirstFood == "completed") {
        console.log($localStorage.foodDetails[0]);
        $scope.mexicanindexNumber0 = false;
        $scope.mexicancompleteImage0 = true;
      }

      if ($localStorage.mSecondFood == "completed") {
        $scope.mexicanindexNumber1 = false;
        $scope.mexicancompleteImage1 = true;
      }

      if ($localStorage.mThirdFood == "completed") {
        $scope.mexicanindexNumber2 = false;
        $scope.mexicancompleteImage2 = true;

      }

      if ($localStorage.mFourthFood == "completed") {
        $scope.mexicanindexNumber3 = false;
        $scope.mexicancompleteImage3 = true;
      }
      if ($localStorage.mFifthFood == "completed") {
        $scope.mexicanindexNumber4 = false;
        $scope.mexicancompleteImage4 = true;
      }
    }




  });



  $scope.setSelection = function(item) {
    $scope.SelectionText = item; // pull selected city using {{selected | json}}
  };

  $scope.FoodDetails = function(data,DataLength) {
    for(var i = 1;i <= DataLength; i++)
    {
       var one = 'FoodDetail' + i;
      if (data === i){
        document.getElementById(one).className = "fti-body";
      }
      else {
        document.getElementById(one).className = "fti-body hide";
      }
    }
  };

  $scope.toggleActive1 = function(btn) {
    $scope.i = 0;
    $scope.isAmericaActive=false;
    $scope.isItalyActive = false;
    $scope.isKoreaActive = false;
    $scope.isJapanActive = false;
    $scope.isMexicoActive = false;
    journeyService.getFoodDetails(btn).then(function (data) {
      $scope.films = data;
      this.dataFood = data;




    });
    if(btn === 'America') {
      $scope.isAmericaActive = true;
      $scope.i=0;
      call($scope.users.americaPercentage);
      $localStorage.abtnName = "America";

    }

    else if(btn === 'Korea' ) {
      $scope.isKoreaActive = true;
      $scope.i=1;
      call($scope.users.koreanPercentage);
      $localStorage.kbtnName = "Korea";
    }
    else if(btn === 'Italy') {
      $scope.isItalyActive =true;
      $scope.i=2;
      call($scope.users.italianPercentage);
      $localStorage.ibtnName = "italy";
    }
    else if(btn === 'Mexico') {
      $scope.isMexicoActive =true;
      $scope.i=3;
      call($scope.users.mexicanPercentage);
      $localStorage.mbtnName = "mexican";
    }
    else if(btn === 'Japan') {
      $scope.isJapanActive =true;
      $scope.i=4;
      call($scope.users.japanesePercentage);
      $localStorage.jbtnName = "japanese";
    }
    else {
      $scope.isAmericaActive = false;
      $scope.isItalyActive = false;
      $scope.isKoreaActive = false;
      $scope.isMexicoActive = false;
      $scope.isJapanActive = false;
      call($scope.users.americaPercentage);
    }
  };


  $scope.toggleActive2 = function() {
    if($scope.isUploadActive) {
      $scope.isUploadActive = !$scope.isUploadActive;
    }
    $scope.isDownloadActive = !$scope.isDownloadActive;
  };

}



function journeyFactory($http,$localStorage) {
  /*jshint validthis:true*/this.$inject = ['$http','$localStorage'];

  function getFoodDetails(btn) {
    if(btn === 'America' ) {


      return $http
        .get('../data/American.json')
        .then(complete)
        .catch(failed);
    }
    else  if(btn === 'Korea' ) {

      return $http
        .get('../data/Indian.json')
        .then(complete)
        .catch(failed);
    }
    else  if(btn === 'Italy' ) {
      return $http
        .get('../data/Italian.json')
        .then(complete)
        .catch(failed);
    }
    else  if(btn === 'Mexico' ) {
      return $http
        .get('../data/Mexico.json')
        .then(complete)
        .catch(failed);
    }
    else if(btn === 'Japan' ) {
      return $http
        .get('../data/Japanese.json')
        .then(complete)
        .catch(failed);
    }
    else{
      return $http
        .get('../data/American.json')
        .then(complete)
        .catch(failed);
    }
  }

  function complete(response) {
    return response.data;
  }

  function failed(error) {
    return error.statusText;
  }

  return {
    getFoodDetails: getFoodDetails
  };
}



angular
  .module("followUp",["ngStorage"])
  .controller('JourneyController', JourneyController)
  .factory('journeyService', journeyFactory)
  .factory('userService', userFactory)
  .directive('detailsInfo',function(){
    return{
      restrict: 'E',
      template: '<div ><div ng-transclude></div></div>',
      transclude: true
    };
  })

    .controller("followUpController", function($scope,$localStorage, $window){
      $localStorage.americanfollowUp="";
      $localStorage.koreanfollowUp="";
      $localStorage.italyfollowUp="";
      $localStorage.mexicofollowUp="";
      $localStorage.japanfollowUp="";
      $scope.americacontinuebtn=true;
      $scope.koreacontinuebtn=true;
      $scope.italycontinuebtn=true;
      $scope.mexicontinuebtn=true;
      $scope.japancontinuebtn=true;

      if($localStorage.americanButton == true){
        $scope.americanButtonText = "Continue";
        $scope.americashowBtn = true;
        $scope.koreacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.mexicontinuebtn=false;
        $scope.japancontinuebtn=false;
      }
      //American
      $scope.americanButtonText="Follow Up";
      $scope.americanfollowUpBtn = function()
      {
        console.log($scope.americanButtonText);
        $scope.americanButtonText = "Continue";
        $localStorage.americanfollowUp = "true";
        console.log($localStorage.americanfollowUp);
        $scope.americashowBtn = true;
        $window.location.href='index-journey-challenge.html';
        $scope.koreacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.mexicontinuebtn=false;
        $scope.japancontinuebtn=false;
        //$window.location.href='index-journey-challenge.html'
        $localStorage.americanButton = true;
        $localStorage.american = true;

      }
      $scope.americanContinueBtn = function()
      {
        //$window.location.href='index-journey-challenge.html';
        $localStorage.americanfollowUp = "false";
        $localStorage.americanButton = false;
        $localStorage.american = false;
        $window.location.reload();
      }
      //Korean

      if($localStorage.koreanbutton == true)
      {
        $scope.koreanshowBtn = true;
        $scope.americacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.mexicontinuebtn=false;
        $scope.japancontinuebtn=false;
      }

      $scope.koreanButtonText="Follow Up";
      $scope.koreanfollowUpBtn = function()
      {
        $scope.koreanButtonText = "Continue";
        $localStorage.koreanfollowUp = "true";
        $scope.koreanshowBtn = true;
        $window.location.href='index-journey-challenge.html'
        $scope.americacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.mexicontinuebtn=false;
        $scope.japancontinuebtn=false;
        $localStorage.koreanbutton=true;
        $localStorage.korean = true;
      }
      $scope.koreanContinueBtn = function()
      {
        $localStorage.koreanfollowUp = "false";
        $localStorage.koreanbutton=false;
        $localStorage.korean = false;
        $window.location.reload();
      }
      //Italy

      if($localStorage.italybutton==true)
      {
        $scope.italyshowBtn = true;
        $scope.americacontinuebtn=false;
        $scope.koreacontinuebtn=false;
        $scope.mexicontinuebtn=false;
        $scope.japancontinuebtn=false;
      }
      $scope.italianButtonText="Follow Up";
      $scope.italyfollowUpBtn = function()
      {
        $scope.italianButtonText = "Continue";
        $localStorage.italyfollowUp = "true";
        $scope.italyshowBtn = true;
        $scope.americacontinuebtn=false;
        $scope.koreacontinuebtn=false;
        $scope.mexicontinuebtn=false;
        $scope.japancontinuebtn=false;
        $window.location.href='index-journey-challenge.html';
        $localStorage.italybutton = true;
        $localStorage.italy = true;
      }
      $scope.italyContinueBtn = function()
      {
        $localStorage.italyfollowUp = "false";
        $localStorage.italybutton=false;
        $localStorage.italy = false;
        $window.location.reload();
      }
      //Mexico

      if($localStorage.mexicanbutton==true)
      {
        $scope.mexicoshowBtn = true;
        $scope.americacontinuebtn=false;
        $scope.koreacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.japancontinuebtn=false;
      }
      $scope.mexicanButtonText="Follow Up";
      $scope.mexicofollowUpBtn = function()
      {
        $scope.mexicanButtonText = "Continue";
        $localStorage.mexicofollowUp = "true";
        $scope.mexicoshowBtn = true;
        $scope.americacontinuebtn=false;
        $scope.koreacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.japancontinuebtn=false;
        $window.location.href='index-journey-challenge.html';
        $localStorage.mexicanbutton=true;
        $localStorage.mexican = true;
      }
      $scope.mexicoContinueBtn = function()
      {
        $localStorage.mexicofollowUp = "false";
        $localStorage.mexicanbutton=false;
        $window.location.reload();
        $localStorage.mexican = false;
      }
      //Japan

      if($localStorage.japanbutton == true)
      {
        $scope.japanshowBtn = true;
        $scope.americacontinuebtn=false;
        $scope.koreacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.mexicontinuebtn=false;
      }

      $scope.japanButtonText="Follow Up";
      $scope.japanfollowUpBtn = function()
      {
        $scope.japanButtonText = "Continue";
        $localStorage.japanfollowUp = "true";
        $scope.japanshowBtn = true;
        $scope.americacontinuebtn=false;
        $scope.koreacontinuebtn=false;
        $scope.italycontinuebtn=false;
        $scope.mexicontinuebtn=false;
        $window.location.href='index-journey-challenge.html';
        $localStorage.japanbutton = true;
        $localStorage.japanese = true;
      }
      $scope.japanContinueBtn = function()
      {
        $localStorage.japanfollowUp = "false";
        $localStorage.japanbutton = false;
        $localStorage.japanese = false;
        $window.location.reload();
      }
    });




