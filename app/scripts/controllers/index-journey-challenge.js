angular
  .module('yelpAPI',["ngStorage"])
  .controller('yelpController',yelpController)

    .factory('yelpService',yelpFactory);

function yelpFactory($http,$localStorage) {
  this.$inject = ['$http','$localStorage'];

  if ($localStorage.american == true){
    return  {
      getDetails: getAmericanDetails
    }
  }
  if ($localStorage.korean == true){
    return {
      getDetails: getKoreanDetails
    }
  }
  if ($localStorage.italy == true){
    return {
      getDetails: getItalyDetails
    }
  }
  if ($localStorage.japanese == true){
    return {
      getDetails: getJapaneseDetails
    }
  }
  if ($localStorage.mexican == true){
    return {
      getDetails: getMexicanDetails
    }
  }



  //return {
  //  getDetails: getAmericanDetails,
  //  getDetails: getKoreanDetails
  //
  //};
  console.log($localStorage.american);
  console.log($localStorage.korean);
  console.log($localStorage.italy);
  console.log($localStorage.japanese);
  console.log($localStorage.mexican);


  if ($localStorage.american == true){
    function getAmericanDetails () {
      return $http
          .get('../data/American.json')
          .then(complete)
          .catch(failed);
    }
  }


  if ($localStorage.korean == "true"){

    function getKoreanDetails () {
      return $http
          .get('../data/Indian.json')
          .then(complete)
          .catch(failed);
    }
  }


  if ($localStorage.italy == true){
    function getItalyDetails () {
      return $http
          .get('../data/Italian.json')
          .then(complete)
          .catch(failed);
    }
  }



  if ($localStorage.japanese == true){
    function getJapaneseDetails () {
      return $http
          .get('../data/Japanese.json')
          .then(complete)
          .catch(failed);
    }
  }



  if ($localStorage.mexican == true){
    function getMexicanDetails () {
      return $http
          .get('../data/Mexico.json')
          .then(complete)
          .catch(failed);
    }
  }





  function complete(response) {
    return response.data;
  }

  function failed(error) {
    return error.statusText;
  }
}

function yelpController($scope, yelpService, $window, $rootScope,$localStorage) {
  this.$inject = ['$scope', 'yelpService','$window','$rootScope','$localStorage'];
  //console.log($localStorage.labelText1);
  //$localStorage.$reset();
  $scope.buttonText = "Mark as Started";
  $scope.backgroundColor = "#fe5140";
  $scope.completeImage = false;
  $scope.indexNumber = true;

  if($localStorage.american == true){
    $scope.navName = "American";
  }
  if($localStorage.korean == true){
    $scope.navName = "Korean";
  }
  if($localStorage.italy == true){
    $scope.navName = "Italian";
  }
  if($localStorage.japanese == true){
    $scope.navName = "Japanese";
  }
  if($localStorage.mexican == true){
    $scope.navName = "Mexican";
  }






  //if(angular.isUndefined($localStorage.labelText1) || $localStorage.labelText1 == "")
  //{
    $scope.labelText = "Started";
  //  console.log($scope.labelText);
  //}
  //else{
  //  $scope.labelText = "Completed";
  //  $scope.markStartButton = true;
  //}

  $scope.nextButtonEnable = true;
  //$localStorage.nextText ="";
  //$localStorage.index=0;
  if($localStorage.nextText == "true"){
    $localStorage.nextText == "true"
  }
  else{
    $localStorage.nextText = "false";
  }



  $scope.nextButton = function () {
    var count = $localStorage.index;
    $localStorage.index = count+1;
    $localStorage.nextText = "true";
    $localStorage.labelText1 = "";
    $window.location.reload();
    }
  console.log($localStorage.index);

  //console.log($localStorage.nextText);
  //if($localStorage.nextText =="false" || $localStorage.nextText=="")
  //{
  //var next = "true";
  //}
  //else{
  //  var next="false";
  //}
  if($localStorage.nextText == "true"){
    $localStorage.nextText == "true"
  }

  console.log($localStorage.nextText);

  if ($localStorage.nextText == "false") {

    yelpService.getDetails().then(function (data) {
      $scope.details = data;
      $localStorage.title = $scope.title = data[0].businesses[0].name;
      $scope.imageURL = data[0].businesses[0].image_url;
      $scope.descHead = data[0].businesses[0].name;
      $scope.descP = data[0].businesses[0].snippet_text;
      $scope.dispPhone = data[0].businesses[0].display_phone;
      $scope.website = data[0].businesses[0].url;
      $scope.number = "1";



      console.log(data[0].businesses[0].location.coordinate.latitude, data[0].businesses[0].location.coordinate.longitude);
      var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(data[0].businesses[0].location.coordinate.latitude, data[0].businesses[0].location.coordinate.longitude),
        mapTypeId: google.maps.MapTypeId.TERRAIN
      }

      $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

      $scope.markers = [];

      var infoWindow = new google.maps.InfoWindow();

      var createMarker = function () {

        var marker = new google.maps.Marker({
          map: $scope.map,
          position: new google.maps.LatLng(data[0].businesses[0].location.coordinate.latitude, data[0].businesses[0].location.coordinate.longitude),

          title: data[0].businesses[0].location.city,


        });
        console.log(position);
        marker.content = '<div class="infoWindowContent">' + data[0].businesses[0].name + '</div>';

        google.maps.event.addListener(marker, 'click', function () {
          infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
          infoWindow.open($scope.map, marker);
        });

        $scope.markers.push(marker);

      }

      var landMark = new google.maps.LatLng(data[0].businesses[0].location.coordinate.latitude, data[0].businesses[0].location.coordinate.longitude);
      createMarker(landMark);


      $scope.openInfoWindow = function (e, selectedMarker) {

        e.preventDefault();
        google.maps.event.trigger(selectedMarker, 'click');
      }

    })
  }
  else {
        yelpService.getDetails().then(function (data) {
          console.log(data);
          console.log(data[0].businesses[0].name);
          console.log("next data::"+data);
          //$scope.details = data;
          $localStorage.title = $scope.title = data[0].businesses[$localStorage.index].name;
          $scope.imageURL = data[0].businesses[$localStorage.index].image_url;
          $scope.descHead = data[0].businesses[$localStorage.index].name;
          $scope.descP = data[0].businesses[$localStorage.index].snippet_text;
          $scope.dispPhone = data[0].businesses[$localStorage.index].display_phone;
          $scope.website = data[0].businesses[$localStorage.index].url;
          $scope.number = $localStorage.index+1;

          console.log($localStorage.firstFood);
          console.log($localStorage.title);
          console.log(data[0].businesses[$localStorage.index].name);



          if($localStorage.title == data[0].businesses[0].name && $localStorage.aFirstFood == "completed"){

            //$scope.labelText = "Completed";
            $scope.markStartButton = true;
            $scope.buttonText = "Way to go! Take the next challenge";
            $scope.backgroundColor = "#F2F2F2";
            $scope.backgroundBorder = "#F2F2F2";
            $scope.text = "black";
            $scope.labelText = "Completed";
            $scope.buttonColor = "#47A3FF";
            $scope.completedButton = true;
            $scope.nextButtonEnable = false;
            $scope.completeImage = true;
            $scope.indexNumber = false;
          }

          if($localStorage.title == data[0].businesses[1].name && $localStorage.aSecondFood == "completed"){
            console.log("--------------"+$localStorage.title);
            //$scope.labelText = "Completed";
            $scope.markStartButton = true;
            $scope.buttonText = "Way to go! Take the next challenge";
            $scope.backgroundColor = "#F2F2F2";
            $scope.backgroundBorder = "#F2F2F2";
            $scope.text = "black";
            $scope.labelText = "Completed";
            $scope.buttonColor = "#47A3FF";
            $scope.completedButton = true;
            $scope.nextButtonEnable = false;
            $scope.completeImage = true;
            $scope.indexNumber = false;
          }

          if($localStorage.title == data[0].businesses[2].name && $localStorage.aThirdFood == "completed"){
            console.log("--------------"+$localStorage.title);
            //$scope.labelText = "Completed";
            $scope.markStartButton = true;
            $scope.buttonText = "Way to go! Take the next challenge";
            $scope.backgroundColor = "#F2F2F2";
            $scope.backgroundBorder = "#F2F2F2";
            $scope.text = "black";
            $scope.labelText = "Completed";
            $scope.buttonColor = "#47A3FF";
            $scope.completedButton = true;
            $scope.nextButtonEnable = false;
            $scope.completeImage = true;
            $scope.indexNumber = false;
          }

          if($localStorage.title == data[0].businesses[3].name && $localStorage.aFourthFood == "completed"){
            console.log("--------------"+$localStorage.title);
            //$scope.labelText = "Completed";
            $scope.markStartButton = true;
            $scope.buttonText = "Way to go! Take the next challenge";
            $scope.backgroundColor = "#F2F2F2";
            $scope.backgroundBorder = "#F2F2F2";
            $scope.text = "black";
            $scope.labelText = "Completed";
            $scope.buttonColor = "#47A3FF";
            $scope.completedButton = true;
            $scope.nextButtonEnable = false;
            $scope.completeImage = true;
            $scope.indexNumber = false;
          }

          if($localStorage.title == data[0].businesses[4].name && $localStorage.aFifthFood == "completed"){
            console.log("--------------"+$localStorage.title);
            //$scope.labelText = "Completed";
            $scope.markStartButton = true;
            $scope.buttonText = "Way to go! Take the next challenge";
            $scope.backgroundColor = "#F2F2F2";
            $scope.backgroundBorder = "#F2F2F2";
            $scope.text = "black";
            $scope.labelText = "Completed";
            $scope.buttonColor = "#47A3FF";
            $scope.completedButton = true;
            $scope.nextButtonEnable = false;
            $scope.completeImage = true;
            $scope.indexNumber = false;
          }


          console.log(data[0].businesses[$localStorage.index].location.coordinate.latitude, data[0].businesses[$localStorage.index].location.coordinate.longitude);
          var mapOptions = {
            zoom: 15,
            center: new google.maps.LatLng(data[0].businesses[$localStorage.index].location.coordinate.latitude, data[0].businesses[$localStorage.index].location.coordinate.longitude),
            mapTypeId: google.maps.MapTypeId.TERRAIN
          }

          $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

          $scope.markers = [];

          var infoWindow = new google.maps.InfoWindow();

          var createMarker = function () {

            var marker = new google.maps.Marker({
              map: $scope.map,
              position: new google.maps.LatLng(data[0].businesses[$localStorage.index].location.coordinate.latitude, data[0].businesses[$localStorage.index].location.coordinate.longitude),

              title: data[0].businesses[$localStorage.index].location.city,


            });
            console.log(position);
            marker.content = '<div class="infoWindowContent">' + data[0].businesses[$localStorage.index].name + '</div>';

            google.maps.event.addListener(marker, 'click', function () {
              infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
              infoWindow.open($scope.map, marker);
            });

            $scope.markers.push(marker);

          }

          var landMark = new google.maps.LatLng(data[0].businesses[$localStorage.index].location.coordinate.latitude, data[0].businesses[$localStorage.index].location.coordinate.longitude);
          createMarker(landMark);


          $scope.openInfoWindow = function (e, selectedMarker) {

            e.preventDefault();
            google.maps.event.trigger(selectedMarker, 'click');
          }

      })
}

  $scope.startButton = function (count) {
    console.log(count)
    if (count >= 1) {
      $scope.markStartButton = true;
      $localStorage.completeButtonText = $scope.buttonText = "Way to go! Take the next challenge";
      $scope.backgroundColor = "#F2F2F2";
      $scope.backgroundBorder = "#F2F2F2";
      $scope.text = "black";
      $scope.labelText = "Completed";
      $scope.buttonColor = "#47A3FF";
      $scope.completedButton = true;
      $scope.nextButtonEnable = false;
      console.log($localStorage.index);

      if($localStorage.index == 0){

        $localStorage.FirstFood = "completed";

      }
      if($localStorage.index == 1){

        $localStorage.SecondFood = "completed";

      }
      if($localStorage.index == 2){

        $localStorage.ThirdFood = "completed";

      }
      if($localStorage.index == 3){

        $localStorage.FourthFood = "completed";

      }
      if($localStorage.index == 4){

        $localStorage.FifthFood = "completed";

      }

      if($localStorage.american == true){
        if($localStorage.index == 0){

          $localStorage.aFirstFood = "completed";

        }
        if($localStorage.index == 1){

          $localStorage.aSecondFood = "completed";

        }
        if($localStorage.index == 2){

          $localStorage.aThirdFood = "completed";

        }
        if($localStorage.index == 3){

          $localStorage.aFourthFood = "completed";

        }
        if($localStorage.index == 4){

          $localStorage.aFifthFood = "completed";

        }
      }

      if($localStorage.korean == true){
        if($localStorage.index == 0){

          $localStorage.kFirstFood = "completed";

        }
        if($localStorage.index == 1){

          $localStorage.kSecondFood = "completed";

        }
        if($localStorage.index == 2){

          $localStorage.kThirdFood = "completed";

        }
        if($localStorage.index == 3){

          $localStorage.kFourthFood = "completed";

        }
        if($localStorage.index == 4){

          $localStorage.kFifthFood = "completed";

        }
      }

      if($localStorage.italy == true){
        if($localStorage.index == 0){

          $localStorage.iFirstFood = "completed";

        }
        if($localStorage.index == 1){

          $localStorage.iSecondFood = "completed";

        }
        if($localStorage.index == 2){

          $localStorage.iThirdFood = "completed";

        }
        if($localStorage.index == 3){

          $localStorage.iFourthFood = "completed";

        }
        if($localStorage.index == 4){

          $localStorage.iFifthFood = "completed";

        }
      }

      if($localStorage.japanese == true){
        if($localStorage.index == 0){

          $localStorage.jFirstFood = "completed";

        }
        if($localStorage.index == 1){

          $localStorage.jSecondFood = "completed";

        }
        if($localStorage.index == 2){

          $localStorage.jThirdFood = "completed";

        }
        if($localStorage.index == 3){

          $localStorage.jFourthFood = "completed";

        }
        if($localStorage.index == 4){

          $localStorage.jFifthFood = "completed";

        }
      }

      if($localStorage.mexican == true){
        if($localStorage.index == 0){

          $localStorage.mFirstFood = "completed";

        }
        if($localStorage.index == 1){

          $localStorage.mSecondFood = "completed";

        }
        if($localStorage.index == 2){

          $localStorage.mThirdFood = "completed";

        }
        if($localStorage.index == 3){

          $localStorage.mFourthFood = "completed";

        }
        if($localStorage.index == 4){

          $localStorage.mFifthFood = "completed";

        }
      }



    }
    else {
      $scope.markStartButton = true;
      $localStorage.markButtonText = $scope.buttonText = "Mark as Completed";
      $scope.backgroundColor = "#47A3FF";
      $scope.backgroundBorder = "#47A3FF";
      $scope.text = "white";
      //$scope.loginBtn = true;
      $scope.labelText = "Started";
    }
  }

  $scope.prevButton = function(){
    //console.log($localStorage.index);
    $localStorage.index = $localStorage.index - 1;
    $localStorage.labelText1 = $localStorage.completeButtonText;

    //console.log($scope.labelText);
    $window.location.reload();
  }



}



