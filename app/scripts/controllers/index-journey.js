/*global $ */
"use strict";
function userFactory($http) {
  /*jshint validthis:true */
  this.$inject = ['$http'];

  function getUser () {
    return $http
      .get('../data/user.json')
      .then(complete)
      .catch(failed);
  }

  function complete(response) {
       return response.data;
  }

  function failed(error) {
    return error.statusText;
  }

  return {
    getUser: getUser
  };
}

angular.module('app',  ["ngStorage"])
  .factory('userService', userFactory)
  .controller('createPropertyCtrl', ['$scope','$window', '$localStorage','userService',
    function($scope, $window,$localStorage,userService) {
      // Insert your code here.
      //$location.path('../index-journey-overview.html');

       userService.getUser().then(function(data) {
        $scope.users = data;

        $('.americaBox').css("width",$scope.users.americaPercentage);
        $('.koreanBox').css("width",$scope.users.koreanPercentage);
        $('.italianBox').css("width",$scope.users.italianPercentage);
        $('.mexicanBox').css("width",$scope.users.mexicanPercentage);
        $('.japanBox').css("width",$scope.users.japanesePercentage);
      });

      $scope.infoClicked = function(message,messagePercentage) {
        //$window.location.href = '../views/index-journey-overview.html';
        $localStorage.message=message;
        $localStorage.messagePercentage=messagePercentage;
        $window.location.href = '../views/index-journey-overview.html';

      };
    }
  ]);
