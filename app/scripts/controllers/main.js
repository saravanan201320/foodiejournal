'use strict';

/**
 * @ngdoc function
 * @name gurgaonAugustTeamApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gurgaonAugustTeamApp
 */
angular.module('gurgaonAugustTeamApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
