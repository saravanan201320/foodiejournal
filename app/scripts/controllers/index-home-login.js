
angular
  .module('start',["ngStorage"])
  .controller('StartController',['$scope',function($scope){
      //$localStorage.$reset();
    $scope.startButton = true;
    $scope.toggle = function() {
      $scope.startButton = $scope.startButton === false ? true: false;

    }
    $scope.showLogin = function(){
      $scope.loginBtn = $scope.loginBtn === false ? true: false;
      //$scope.loginBtn = true;
    }
  }])
  .factory('loginService', loginFactory)
  .controller('validateCtrl', ['$scope', '$window', 'loginService','$localStorage', function($scope, $window, loginService, $localStorage ){
//      $scope.custom = true;
    loginService.getUser().then(function(data) {
      $scope.users = data;
    })
    $scope.userValidation = function() {
      var user = $scope.users.name
      var password = $scope.users.password

      if($scope.user == user && $scope.password == password){
        console.log($localStorage.followUp);
        if($localStorage.followUp == "true"){
          $window.location.href = '../views/index-journey-overview.html';
        }
        else{
          $window.location.href = '../views/index-journey.html';
        }

      }
      else{
        $window.location.href='../404.html';
      }
    }
  }]);

function loginFactory($http) {
  this.$inject = ['$http'];

  return {
    getUser: getUser
  };

  function getUser () {
    return $http
      .get('../data/user.json')
      .then(complete)
      .catch(failed);
  }

  function complete(response) {
    return response.data;
  }

  function failed(error) {
    return error.statusText;
  }
}

